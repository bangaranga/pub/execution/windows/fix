# misinformation brought to you by:
 

## throwing warning with local file
`https://woshub.com/how-to-disable-open-file-security-warnings-in-windows-7/`                 
> https://woshub.com/how-to-disable-open-file-security-warnings-in-windows-7/              


***

- at the very bottom is this             


You can completely disable the “Open File – Security Warning” window for unsafe files using the GPO option Turn off the Security Settings Check feature (located in the section Computer Configuration -> Administrative Templates -> Windows Components -> Internet Explorer).                  


Or you can allow any file to run without displaying the “Open File Security Warning” using the following commands:                         


```
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Internet Explorer\Security" /V "DisableSecuritySettingsCheck" /T "REG_DWORD" /D "00000001" /F
REG ADD "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\3" /V "1806" /T "REG_DWORD" /D "00000000" /F
REG ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\3" /V "1806" /T "REG_DWORD" /D "00000000" /F
```                   


***










